#[derive(Debug)]
pub struct EchoSettings {
    pub delay: jack::Frames,
    pub decay_factor: f32,
}
