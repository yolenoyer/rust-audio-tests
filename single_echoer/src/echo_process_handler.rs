use crossbeam_channel::Receiver;
use anyhow::{anyhow, Result};

use common::{
    ring_buffer::RingBuffer,
    ports::{PortType, get_port_name},
    util::{gain_to_factor, time_to_frames},
};
use crate::{
    app_message::AppMessage,
    echo_settings::EchoSettings,
};

pub struct EchoProcessHandler {
    rx: Receiver<AppMessage>,
    nb_ports: usize,
    input_ports: Vec<jack::Port<jack::AudioIn>>,
    output_ports: Vec<jack::Port<jack::AudioOut>>,
    echo_settings: EchoSettings,
    buffer_size: usize,
    echo_buffers: Vec<RingBuffer<f32>>,
    elapsed: u64,
}

impl EchoProcessHandler {
    pub fn new(client: &jack::Client, nb_ports: usize, rx: Receiver<AppMessage>) -> Result<Self> {
        macro_rules! register_port {
            ($port_type:expr, $spec:expr, $i:expr) => {
                client.register_port(&get_port_name($port_type, nb_ports, $i), $spec)
                    .map_err(|_| anyhow!("Unable to register a Jack port"))
            };
        }
        let input_ports = (0..nb_ports).map(|i| {
            register_port!(PortType::Input, jack::AudioIn::default(), i)
        }).collect::<Result<Vec<_>>>()?;
        let output_ports = (0..nb_ports).map(|i| {
            register_port!(PortType::Output, jack::AudioOut::default(), i)
        }).collect::<Result<Vec<_>>>()?;

        let echo_settings = EchoSettings {
            delay: 44000,
            decay_factor: gain_to_factor(crate::INITIAL_DECAY),
        };

        let echo_buffers: Vec<RingBuffer<f32>> = vec![RingBuffer::new(0); nb_ports];
        let elapsed: u64 = 0;

        let mut process_handler = Self {
            rx,
            nb_ports,
            input_ports,
            output_ports,
            echo_settings,
            buffer_size: 0,
            echo_buffers,
            elapsed,
        };

        process_handler.reset_echo_buffers(client.buffer_size() as usize);

        Ok(process_handler)
    }

    fn reset_echo_buffers(&mut self, buffer_size: usize) {
        self.buffer_size = buffer_size;
        let echo_buf_size = buffer_size + self.echo_settings.delay as usize;
        for port in 0..self.nb_ports {
            self.echo_buffers[port] = RingBuffer::new(echo_buf_size);
        }
        self.elapsed = 0;
    }
}

impl jack::ProcessHandler for EchoProcessHandler {
    fn process(&mut self, client: &jack::Client, ps: &jack::ProcessScope) -> jack::Control {
        while let Ok(message) = self.rx.try_recv() {
            match message {
                AppMessage::SetDelay(d) => {
                    self.echo_settings.delay = time_to_frames(client.sample_rate(), d / 1000.0);
                    self.reset_echo_buffers(self.buffer_size);
                },
                AppMessage::SetDecayFactor(f) => {
                    self.echo_settings.decay_factor = f;
                },
            }
        }

        let echo_duration = {
            let delay = self.echo_settings.delay as u64;
            if delay < self.elapsed + self.buffer_size as u64 {
                Some(if delay < self.elapsed {
                    self.buffer_size
                } else {
                    self.buffer_size - (delay - self.elapsed) as usize
                })
            } else {
                None
            }
        };

        for port in 0..self.nb_ports {
            let input_slice = self.input_ports[port].as_slice(ps);
            let output_slice = self.output_ports[port].as_mut_slice(ps);

            output_slice.clone_from_slice(&input_slice);
            self.echo_buffers[port].push_slice(&input_slice).unwrap();

            if let Some(duration) = echo_duration {
                let input_iter = self.echo_buffers[port].iter().take(duration);
                let output_iter = output_slice[self.buffer_size - duration..].iter_mut();
                for (input, output) in input_iter.zip(output_iter) {
                    *output += *input * self.echo_settings.decay_factor;
                }
                self.echo_buffers[port].pop_slice(duration).unwrap();
            }
        }

        self.elapsed += self.buffer_size as u64;

        jack::Control::Continue
    }

    fn buffer_size(&mut self, _: &jack::Client, size: jack::Frames) -> jack::Control {
        println!("Buffer size: {}", size);
        self.reset_echo_buffers(size as usize);
        jack::Control::Continue
    }
}
