use crossbeam_channel::{bounded, Receiver};
use anyhow::{anyhow, Result};

mod app_message;
mod gui;
mod echo_settings;
mod echo_process_handler;

use common::jack::{
    new_jack_client,
    JackNotifications,
};
use app_message::AppMessage;
use gui::run_gtk_application;
use echo_process_handler::EchoProcessHandler;

const NB_PORTS: usize = 2;
const INITIAL_DECAY: f64 = -6.0;

fn start_jack(nb_ports: usize, rx: Receiver<AppMessage>)
    -> Result<jack::AsyncClient<JackNotifications, EchoProcessHandler>>
{
    let client = new_jack_client("Single echoer")?;
    let process = EchoProcessHandler::new(&client, nb_ports, rx)?;

    // Activate the client, which starts the audio processing.
    client.activate_async(JackNotifications, process)
        .map_err(|_| anyhow!("Unable to activate the jack client"))
}

fn main() -> Result<()> {
    let (tx, rx) = bounded::<AppMessage>(1000);

    // Start the audio processing.
    let active_client = start_jack(NB_PORTS, rx)?;

    // Start and run the gui.
    run_gtk_application(tx);

    // Bye.
    active_client.deactivate().unwrap();
    Ok(())
}
