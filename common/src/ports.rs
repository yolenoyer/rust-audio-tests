#[derive(Debug)]
pub enum PortType {
    Input,
    Output,
}

pub fn get_port_name(port_type: PortType, nb_ports: usize, port: usize) -> String {
    if port >= nb_ports {
        panic!("Wrong port number: {}", port);
    }
    let mut name = String::from(match port_type {
        PortType::Input => "In",
        PortType::Output => "Out",
    });
    match nb_ports {
        1 => (),
        2 => {
            name.push(' ');
            name.push(match port {
                0 => 'L',
                1 => 'R',
                _ => unreachable!(),
            });
        },
        _ => {
            name.push_str(&format!(" {}", port + 1));
        },
    };
    name
}
